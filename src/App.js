import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link
}from 'react-router-dom';
import './App.css';


//components
import Header from './components/header';
import Footer from './components/footer';
import Homepage from './components/pages/homePage';
import Products from './components/pages/products';
import About from './components/pages/about';
import Contact from './components/pages/contact';

class App extends Component{
    render(){
        return (
             <Router>
            <div className ="App">
              
             <Header />
               
              <Route exact path='/' component={Homepage} />
            <Route exact path='/Products' component={Products }/>
            <Route exact path='/About' component={About }/>
            <Route exact path='/Contact' component={Contact }/>
            
             <Footer />
            </div>
            </Router>
        );
    }
}
export default App;
